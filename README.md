# 悬赏App服务端

#### 介绍
PHP写的仿某悬赏任务App，PHP代码开源

#### 展示图片
![App首页](https://images.gitee.com/uploads/images/2020/1215/174924_121188a3_5096724.png "屏幕快照 2020-12-15 下午5.48.17.png")
![推广页面](https://images.gitee.com/uploads/images/2020/1215/175012_02039f9a_5096724.png "屏幕快照 2020-12-15 下午5.48.49.png")
![用户中心](https://images.gitee.com/uploads/images/2020/1215/175030_cb30f029_5096724.png "屏幕快照 2020-12-15 下午5.49.09.png")

#### 访问地址

[手机端访问地址：https://m.task.kechuang.link](https://m.task.kechuang.link/#/pages/index/index?share_code=X3YS75)

[管理后台访问地址：https://task.kechuang.link/admin](https://task.kechuang.link/admin) 账户：demo888 密码：123456

[安卓APP下载地址：https://static.kechuang.link/task/download/android/task-1.0.apk](https://static.kechuang.link/task/download/android/task-1.0.apk) 注意：不要在微信浏览器打开，微信不允许直接下载


#### 更多

    前端、App 代码需要加我微信谈 jackhe1688